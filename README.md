# omet

Bei meiner Arbeit als Kinotechniker belastete mich der wiederkehrende wöchentliche Aufwand, den wir für das **Konvertieren des Bild- und Tonmaterials** der Kinowerbung in den Kinostandard [Digital-Cinema-Package](https://en.wikipedia.org/wiki/Digital_Cinema_Package) benötigten. Auch die **Kommunikation** mit den Werbevermarktern über Buchungsdauer und Ort der Werbespots war aufwändig und fehleranfällig.

Ich erkannte das Potenzial einer **Plattform** als **Schnittstelle** zwischen Werbevermarktern und Kinobetrieben. Die Plattform erlaubt den Werbevermarktern die Werbung **eigenständig** zu buchen. Das Bild- und Tonmaterial wird **automatisch** geprüft und in ein [Digital-Cinema-Package](https://en.wikipedia.org/wiki/Digital_Cinema_Package) konvertiert.

Mit dieser Motivation eignete ich mir als Quereinsteiger das Wissen für die Entwicklung dieser Plattform an. Programmieren in **Javascript** auf dem Server in der Umgebung **NodeJS** und auf dem Client im **Webbrowser**, Design von **relationalen Datenbanken** in **SQL** und **DevOps**-Aufgaben wie **Deployment** und **Monitoring** erarbeitet ich mir Schritt für Schritt.

[OmET](https://omet.bildwurf.ch/) war von 2017 bis 2019 bei den [Arthouse Kinos in Zürich](https://www.arthouse.ch/) und [Bildwurf](https://www.bildwurf.ch/) im Einsatz, brachte grosse Einsparungen bei der benötigten Zeit für die Werbebuchungen und erhöhte die Zuverlässigkeit durch Automation.

## Architektur

![diagram architecture omet](./architektur.png)

### Backend
[https://gitlab.com/hobbes/cinedia-apiserver](https://gitlab.com/hobbes/cinedia-apiserver)

REST API with express.js.

### Frontend
[https://gitlab.com/hobbes/cinedia-webapp](https://gitlab.com/hobbes/cinedia-webapp)

SPA with Vue.js

![screencapture booking of sujet](book.gif)
![screencapture move of sujet in a block](./move.gif)


